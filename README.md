This is an iOS application created to demonstrate the capabilities of geolocation.

It uses the Model-View-Presenter pattern:
- The presenter is responsible for the orchestration of work of all the services and sends the delegate calls, in this case, the view controller to show view events.

The delegate pattern is used in this application because we are making heavy use of CoreLocation which uses the delegate patterns.

