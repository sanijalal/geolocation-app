//
//  UserInfoStorageServiceTests.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import XCTest
@testable import Geolocation

struct ItemToTest: Codable {
    let name: String
    let floatValue: Float
    let optionalCallsign: String?
}

class UserInfoStorageServiceTests: XCTestCase {
    let storageKey = "satay"

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        UserDefaults.standard.removeObject(forKey: storageKey)
    }

    func testObjectIsStoredInUserDefaults() {
        let item = ItemToTest(name: "Hello", floatValue: 1.2, optionalCallsign: nil)
        let service = UserInfoStorageService()
        
        let storeStatus = service.store(item: item, key: storageKey)
        
        XCTAssertTrue(storeStatus)
        
        if let savedData = UserDefaults.standard.object(forKey: storageKey) as? Data {
            if let savedItem = try? JSONDecoder().decode(ItemToTest.self, from: savedData) {
                XCTAssertTrue(item.name == savedItem.name)
                XCTAssertTrue(item.floatValue == savedItem.floatValue)
                XCTAssertTrue(item.optionalCallsign == savedItem.optionalCallsign)
            } else {
                XCTFail("Item not parsed from user defaults.")
            }
        }
    }

    func testObjectIsRetrievedFromUserDefaults() {
        let item = ItemToTest(name: "HelloMatey", floatValue: 2.4, optionalCallsign: "Crocodila")
        if let encoded = try? JSONEncoder().encode(item) {
            UserDefaults.standard.set(encoded, forKey: storageKey)
        }
        
        let service = UserInfoStorageService()
        
        let itemRetrieved : ItemToTest = service.retrieve(key: storageKey)!
        
        XCTAssertTrue(item.name == itemRetrieved.name)
        XCTAssertTrue(item.floatValue == itemRetrieved.floatValue)
        XCTAssertTrue(item.optionalCallsign == itemRetrieved.optionalCallsign)
    }
    

}
