//
//  GeofenceServiceTests.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import XCTest
@testable import Geolocation

class GeofenceServiceTests: XCTestCase {

    func testGeofenceIsStoredToStorage() {
        let storage = MockStorage()
        let service = GeofenceService(storage: storage)
        
        let fence = GeoFence(wifiName: "halilintar", radius: 2.0, latitude: 1.2, longitude: 1.4)
        
        XCTAssertTrue(service.save(fence: fence))
    }
    
    func testGeofenceIsRetrievedFromStorageIfStorageHasGeofence() {
        let storage = MockStorage()
        let fence = GeoFence(wifiName: "halilintar", radius: 2.0, latitude: 1.2, longitude: 1.4)
        
        storage.returnItem = fence
        
        let service = GeofenceService(storage: storage)
        let fenceRetrieved = service.get()
        
        XCTAssertTrue(fence.wifiName == fenceRetrieved?.wifiName)
        XCTAssertTrue(fence.radius == fenceRetrieved?.radius)
        XCTAssertTrue(fence.latitude == fenceRetrieved?.latitude)
        XCTAssertTrue(fence.longitude == fenceRetrieved?.longitude)
    }
    
    func testGeofenceIsNilFromStorageIfStorageHasNoGeofence() {
        let storage = MockStorage()
        storage.returnItem = nil
        
        let service = GeofenceService(storage: storage)
        let fenceRetrieved = service.get()
        
        XCTAssertNil(fenceRetrieved)
    }

    

}
