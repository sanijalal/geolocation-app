//
//  MockStorage.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
@testable import Geolocation

struct MockStorageItem: Codable {
    let name: String
    let floatValue: Float
}

class MockStorage: StorageAble {
    let emptyKey = "emptyKey"
    let validKey = "geofence"
    var returnItem: GeoFence?
    
    func createMockItem() -> MockStorageItem {
        return MockStorageItem(name: "Hello", floatValue: 1.2)
    }
    
    func store<T>(item: T, key: String) -> Bool where T : Decodable, T : Encodable {
        return true
    }
    
    func retrieve<T>(key: String) -> T? where T : Decodable, T : Encodable {
        if key == emptyKey {
            return nil
        } else if key == validKey {
            guard let item = returnItem else {
                return nil
            }
            return item as? T
        }
        return nil
    }
}

