//
//  LocationCheckPresenterDelegateSpy.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
import XCTest
@testable import Geolocation

enum LocationCheckDelegateCallType {
    case locationDetermined
    case noGeofenceFound
    case isChecking
    case isFinishedChecking
}

class LocationCheckPresenterDelegateSpy: LocationCheckPresenterDelegate {
    
    init() {
        delegateCalls = []
    }
    
    var asyncExpectation: XCTestExpectation?
    var delegateCalls: [LocationCheckDelegateCallType]?
    
    func locationDetermined(state: GeoLocationState) {
        guard let expectation = asyncExpectation else {
          XCTFail("Delegate was not setup correctly. Missing XCTExpectation reference")
          return
        }
        
        delegateCalls?.append(.locationDetermined)
        expectation.fulfill()
    }
    
    func noGeofenceFound() {
        guard let expectation = asyncExpectation else {
          XCTFail("Delegate was not setup correctly. Missing XCTExpectation reference")
          return
        }
        
        delegateCalls?.append(.noGeofenceFound)
        expectation.fulfill()
    }
    
    func isChecking() {
        guard let expectation = asyncExpectation else {
          XCTFail("Delegate was not setup correctly. Missing XCTExpectation reference")
          return
        }
        
        delegateCalls?.append(.isChecking)
        expectation.fulfill()
    }
    
    func isFinishedChecking() {
        guard let expectation = asyncExpectation else {
          XCTFail("Delegate was not setup correctly. Missing XCTExpectation reference")
          return
        }
        
        delegateCalls?.append(.isFinishedChecking)
        expectation.fulfill()
    }

}
