//
//  LocationCheckPresenterTests.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import XCTest
@testable import Geolocation

class LocationCheckPresenterTests: XCTestCase {

    private var geolocationService: GeoLocationService?
    
    override func setUp() {
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNoGeofenceDelegateMethodIsCalledWhenNoGeofenceAvailable() {
        let storage = MockStorage()
        storage.returnItem = nil
        
        let geofenceService = GeofenceService(storage: storage)
        let geolocationService = GeoLocationService(locationManager: MockLocationManager())
        
        
        let presenter = LocationCheckPresenter(geofence: geofenceService,
                                               geolocation: geolocationService,
                                               wifiCheckService: WifiCheckService())
        
        let delegateSpy = LocationCheckPresenterDelegateSpy()
        presenter.delegate = delegateSpy
        
        let expect = expectation(description: "Delegate is called")
        delegateSpy.asyncExpectation = expect
        
        presenter.startCheckForLocationInsideGeoFence()
        
        waitForExpectations(timeout: 1.0) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertTrue(delegateSpy.delegateCalls?.count == 1)
            XCTAssertTrue(delegateSpy.delegateCalls == [.noGeofenceFound])
        }
    }

}
