//
//  MockLocationManager.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
import CoreLocation
@testable import Geolocation

class MockLocationManager: LocationManager {
    func requestLocation() {
        let locations = [CLLocation(latitude: 12.0, longitude: 0.38)]
        delegate?.locationManager?(locationManager, didUpdateLocations: locations)
    }
    
    func stopUpdatingLocation() {
        // Stop. We are not doing anything real.
    }
    
    func requestWhenInUseAuthorization() {
        delegate?.locationManager?(locationManager, didChangeAuthorization: .authorizedAlways)
    }
    
    func authorizationStatus() -> CLAuthorizationStatus {
        return .authorizedAlways
    }
    
    
    func setDelegate(locationDelegate: CLLocationManagerDelegate) {
        self.delegate = locationDelegate
    }

    init() {
        self.locationManager = CLLocationManager.init()
        self.currentStateResponse = .unknown
    }
    
    var locationManager: CLLocationManager
    var delegate: CLLocationManagerDelegate?
    private var currentStateResponse: CLRegionState
    
    func setStateResponse(state: CLRegionState) {
        currentStateResponse = state
    }
    
    func requestState(for: CLRegion) {
        delegate?.locationManager?(locationManager,
                                   didDetermineState: currentStateResponse,
                                   for: CLRegion.init())
    }
}
