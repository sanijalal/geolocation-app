//
//  GeoLocationServiceTests.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import XCTest
import CoreLocation
@testable import Geolocation

class GeoLocationServiceTests: XCTestCase {

    func testDelegateGetsInWifiWhenWifiNameMatches() {
        let wifiNameToTest = "chicken"
        let fence = GeoFence(wifiName: wifiNameToTest, radius: 0, latitude: 11, longitude: 11)
        
        let delegate = GeoLocationServiceDelegateSpy()
        
        let expect = expectation(description: "Delegate is called")
        delegate.asyncExpectation = expect
        
        let locationManager = MockLocationManager()

        let service = GeoLocationService(locationManager: locationManager)
        service.delegate = delegate
        
        service.startCheckingLocation(fence: fence,
                                      wifiName: wifiNameToTest)
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertTrue(delegate.state == .inWifi)
        }
    }
    
    func testDelegateGetsInLocationWhenLocationManagerReturnsInside() {
        let fence = GeoFence(wifiName: "chicken", radius: 0, latitude: 11, longitude: 11)
        
        let delegate = GeoLocationServiceDelegateSpy()
        
        let expect = expectation(description: "Delegate is called")
        delegate.asyncExpectation = expect
        
        let locationManager = MockLocationManager()
        locationManager.setStateResponse(state: .inside)

        let service = GeoLocationService(locationManager: locationManager)
        service.delegate = delegate
        
        service.startCheckingLocation(fence: fence,
                                      wifiName: "curry")
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertTrue(delegate.state == .inLocation)
        }
    }
    
    func testDelegateGetsOutsideLocationWhenLocationManagerReturnsOutside() {
        let fence = GeoFence(wifiName: "chicken", radius: 0, latitude: 11, longitude: 11)
        
        let delegate = GeoLocationServiceDelegateSpy()
        
        let expect = expectation(description: "Delegate is called")
        delegate.asyncExpectation = expect
        
        let locationManager = MockLocationManager()
        locationManager.setStateResponse(state: .outside)

        let service = GeoLocationService(locationManager: locationManager)
        service.delegate = delegate
        
        service.startCheckingLocation(fence: fence,
                                      wifiName: "curry")
        
        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
            XCTAssertTrue(delegate.state == .outsideLocation)
        }
    }

}
