//
//  GeoLocationServiceSpy.swift
//  GeolocationTests
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//
import XCTest
import Foundation
import CoreLocation
@testable import Geolocation

class GeoLocationServiceDelegateSpy: GeoLocationServiceDelegate {
    
    func didUpdateLocation(location: CLLocation?) {
        
    }
    
    func failToUpdateLocation(state: GeoLocationState) {
        
    }
    
    var asyncExpectation: XCTestExpectation?
    var state: GeoLocationState = .isUnknown
    
    func locationDetermined(state: GeoLocationState) {
        guard let expectation = asyncExpectation else {
          XCTFail("Delegate was not setup correctly. Missing XCTExpectation reference")
          return
        }
        
        self.state = state
        expectation.fulfill()
    }
}
