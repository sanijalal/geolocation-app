//
//  GeofenceDetailsModel.swift
//  Geolocation
//
//  Created by Sani on 2/23/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation

public enum GeofenceDetailsModelState {
    case gettingLocation
    case creatingGeofence
    case geofenceAvailable
    case initialised
    case idle
}

public class GeofenceDetailsModel {
    var geofence: GeoFence?
    var state : GeofenceDetailsModelState = .initialised
}
