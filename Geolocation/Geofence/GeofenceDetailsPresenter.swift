//
//  GeofenceDetailsPresenter.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
import CoreLocation

protocol GeofenceDetailsPresenterDelegate {
    func isLoadingLocation()
    func isLoadingDone()
    func locationDenied()
    func showGeofence(fence: GeoFence)
    func showRadiusEdit()
    func showErrorMessage(errorMessage: String)
}

public class GeofenceDetailsPresenter {
    var delegate: GeofenceDetailsPresenterDelegate?
    let geofenceService: GeofenceService
    let geolocationService: GeoLocationService
    let wifiService: WifiCheckService
    let model: GeofenceDetailsModel
    
    init(geofenceService: GeofenceService, geolocationService: GeoLocationService, wifiService: WifiCheckService) {
        self.geofenceService = geofenceService
        self.geolocationService = geolocationService
        self.wifiService = wifiService
        self.model = GeofenceDetailsModel()
        
        self.geolocationService.delegate = self
    }
    
    func getCurrentLocation () {
        geolocationService.startGetCurrentLocationInfo()
    }
    
    func getWifiSSID () -> String? {
        return wifiService.getSSID()
    }
    
    func saveCurrentFence () {
        guard let fence = model.geofence else {
            return
        }
        
        _ = geofenceService.save(fence: fence)
    }
    
    func actionButtonPressed () {
        if model.geofence == nil {
            model.state = .gettingLocation
            delegate?.isLoadingLocation()
            
            switch geolocationService.getAuthorizationStatus() {
            case .authorizedAlways, .authorizedWhenInUse:
                geolocationService.startGetCurrentLocationInfo()
            default:
                geolocationService.requestWhenInUseAuthorization()
            }
            return
        }
    }
    
    func stepperValueChanged (value: Double) {
        
        guard let fence = model.geofence else {
            return
        }
        
        let newFence = GeoFence(wifiName: fence.wifiName,
                                radius: value,
                                latitude: fence.latitude,
                                longitude: fence.longitude)
        model.geofence? = newFence
    }
}

extension GeofenceDetailsPresenter: GeoLocationServiceDelegate {
    func permissionGiven() {
        switch model.state {
        case .gettingLocation:
            geolocationService.startGetCurrentLocationInfo()
        default:
            delegate?.showErrorMessage(errorMessage: "Permission given")
        }
    }
    
    func failToUpdateLocation(state: GeoLocationState) {
        switch state {
        case .noPermission:
            delegate?.locationDenied()
        default:
            delegate?.showErrorMessage(errorMessage: "Failed to update location")
        }
    }
    
    func locationDetermined(state: GeoLocationState) {
        
    }
    
    func didUpdateLocation(location: CLLocation?) {
        guard let validLocation = location else {
            model.state = .idle
            delegate?.showErrorMessage(errorMessage: "Failed to get location")
            return
        }
        
        let ssid = getWifiSSID()
        
        let fence = GeoFence(wifiName: ssid, radius: 1.0, latitude: validLocation.coordinate.latitude, longitude: validLocation.coordinate.longitude)
        
        model.state = .geofenceAvailable
        model.geofence = fence
        saveCurrentFence()
        delegate?.showGeofence(fence: fence)
    }
}
