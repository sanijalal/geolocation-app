//
//  GeofenceDetailsViewController.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import UIKit
import CoreLocation

class GeofenceDetailsViewController: UIViewController {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var latitudeTextField: UITextField!
    @IBOutlet weak var longitudeTextField: UITextField!
    
    @IBAction func radiusDetailButtonPressed(_ sender: Any) {
    }
    @IBOutlet weak var radiusValueLabel: UILabel!
    @IBOutlet weak var buttonBottom: UIButton!
    
    @IBOutlet weak var ssidTextField: UITextField!
    
    let geofenceDetailsPresenter: GeofenceDetailsPresenter
    
    init(presenter: GeofenceDetailsPresenter) {
        self.geofenceDetailsPresenter = presenter
        super.init(nibName: "GeofenceDetailsViewController", bundle: nil)
        presenter.delegate = self
        self.title = "Geofence"
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let segmentBarItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(topRightButtonClicked(sender:)))
        navigationItem.rightBarButtonItem = segmentBarItem
    }

    @objc func topRightButtonClicked(sender: UIBarButtonItem) {
        self.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func bottomButtonPressed(_ sender: Any) {
        geofenceDetailsPresenter.actionButtonPressed()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        let stepValue = Double(sender.value)
        
    }
    
}

extension GeofenceDetailsViewController: GeofenceDetailsPresenterDelegate {
    func isLoadingDone() {
        buttonBottom.isEnabled = true
    }
    
    func showErrorMessage(errorMessage: String) {
        if self.view == nil {
            return
        }
        
        statusLabel.text = errorMessage
        statusLabel.backgroundColor = .red
    }
    
    func showRadiusEdit() {
        
    }
    
    func showGeofence(fence: GeoFence) {
        latitudeTextField.text = "\(fence.latitude)"
        longitudeTextField.text = "\(fence.longitude)"
        
        radiusValueLabel.text = "\(fence.radius)"
        ssidTextField.text = fence.wifiName
    }
    
    func isLoadingLocation() {
        statusLabel.text = "Retrieving location"
        statusLabel.backgroundColor = .green
        buttonBottom.isEnabled = false
    }
    
    func locationDenied() {
        statusLabel.text = "Location Denied"
        statusLabel.backgroundColor = .red
        buttonBottom.isEnabled = true
    }
    
    
}
