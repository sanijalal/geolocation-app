//
//  LocationCheckPresenter.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
import CoreLocation

public protocol LocationCheckPresenterDelegate {
    func locationDetermined(state: GeoLocationState)
    func noGeofenceFound()
    func isChecking()
    func isFinishedChecking()
    func geofenceFound()
    func presentGeofenceDetails()
}

public class LocationCheckPresenter{
    let geofenceService: GeofenceService
    let geolocationService: GeoLocationService
    let wifiCheckService: WifiCheckService
    var delegate: LocationCheckPresenterDelegate?
    let model: LocationCheckModel
    
    init(geofence: GeofenceService, geolocation: GeoLocationService, wifiCheckService: WifiCheckService) {
        self.wifiCheckService = wifiCheckService
        self.geofenceService = geofence
        self.geolocationService = geolocation
        self.model = LocationCheckModel()
        
        self.geolocationService.delegate = self
    }
    
    func startCheckForLocationInsideGeoFence() {
        guard let geoFence = model.geofence else {
            delegate?.noGeofenceFound()
            return
        }
        
        delegate?.isChecking()
        let ssid = wifiCheckService.getSSID()
        geolocationService.startCheckingLocation(fence: geoFence, wifiName: ssid)
    }
    
    func loadStoredFence() {
        delegate?.isChecking()
        if let fence = geofenceService.get() {
            model.geofence = fence
            delegate?.geofenceFound()
        } else {
            delegate?.noGeofenceFound()
        }
    }
    
    func actionButtonPressed () {
        if (model.geofence != nil) {
            startCheckForLocationInsideGeoFence()
            return
        }
        
        // No geofence, present details controller
        delegate?.presentGeofenceDetails()
    }
}

extension LocationCheckPresenter: GeoLocationServiceDelegate {
    func permissionGiven() {
        
    }
    
    func failToUpdateLocation(state: GeoLocationState) {
        
    }
    
    func didUpdateLocation(location: CLLocation?) {
        
    }
    
    func locationDetermined(state: GeoLocationState) {
        delegate?.isFinishedChecking()
        delegate?.locationDetermined(state: state)
    }
}
