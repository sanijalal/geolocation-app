//
//  LocationCheckViewController.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import UIKit
import CoreLocation

class LocationCheckViewController: UIViewController {

    let presenter: LocationCheckPresenter!
    
    @IBOutlet weak var statusLabel: UILabel!
    
    convenience init() {
        let geofenceService = GeofenceService(storage: UserInfoStorageService())
        let presenter = LocationCheckPresenter(geofence: geofenceService,
                                               geolocation: GeoLocationService(locationManager: CLLocationManager.init()),
                                               wifiCheckService: WifiCheckService())
        self.init(presenter: presenter)
    }
    
    init(presenter: LocationCheckPresenter) {
        self.presenter = presenter
        super.init(nibName: "LocationCheckViewController", bundle: nil)
        presenter.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        presenter.loadStoredFence()
    }

    @IBAction func startButtonPressed(_ sender: Any) {
        presenter.actionButtonPressed()
    }
    
    func showIsInside() {
        statusLabel.text = "You are inside the geofence!"
    }
    
    func showIsOutside() {
        statusLabel.text = "You are outside the geofence!"
    }
    
    func showIsChecking() {
        statusLabel.text = "Checking to see if you are inside geofence!"
    }
    
    func presentGeofenceDetailsViewController() {
        
        let presenter = GeofenceDetailsPresenter(geofenceService: GeofenceService(storage: UserInfoStorageService()),
                                                 geolocationService: GeoLocationService(locationManager: CLLocationManager.init()),
                                                 wifiService: WifiCheckService())
        let geofenceDetailsViewController = GeofenceDetailsViewController(presenter: presenter)
        let navigationController = UINavigationController(rootViewController: geofenceDetailsViewController)
        
        self.present(navigationController, animated: true) {
            
        }
    }

}

extension LocationCheckViewController: LocationCheckPresenterDelegate {
    func presentGeofenceDetails() {
        presentGeofenceDetailsViewController()
    }
    
    func geofenceFound() {
        statusLabel.text = "Geofence found. Press button to check if you are inside stored geofence."
    }
    
    func locationDetermined(state: GeoLocationState) {
        
    }
    
    func noGeofenceFound() {
        statusLabel.text = "No geofence found!"
    }
    
    func isChecking() {
        showIsChecking()
    }
    
    func isFinishedChecking() {
        
    }
    
    
}
