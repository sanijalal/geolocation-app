//
//  StorageAble.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation

protocol StorageAble {
    func store<T: Codable>(item: T, key: String) -> Bool
    func retrieve<T:Codable>(key: String) -> T?
}
