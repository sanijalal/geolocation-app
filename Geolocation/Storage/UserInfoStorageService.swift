//
//  UserInfoStorageService.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation

public class UserInfoStorageService : StorageAble {
    func store<T: Codable> (item: T, key: String) -> Bool {
        if let encoded = try? JSONEncoder().encode(item) {
            UserDefaults.standard.set(encoded, forKey: key)
            return true
        }
        
        return false
    }
    
    func retrieve<T: Codable> (key: String) -> T? {
        if let savedData = UserDefaults.standard.object(forKey: key) as? Data {
            if let item = try? JSONDecoder().decode(T.self, from: savedData) {
                return item
            }
        }
        
        return nil
    }
    
    
}
