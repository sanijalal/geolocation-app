//
//  Geofence.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
import CoreLocation

public struct GeoFence: Codable {
    let wifiName: String?
    let radius: Double
    let latitude: Double
    let longitude: Double
}
