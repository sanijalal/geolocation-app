//
//  GeolocationService.swift
//  Geolocation
//
//  Created by Sani on 2/21/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
import CoreLocation

public enum GeoLocationState {
    case inWifi
    case inLocation
    case outsideLocation
    case noPermission
    case isUnknown
}

protocol GeoLocationServiceDelegate: class {
    func locationDetermined(state: GeoLocationState)
    func didUpdateLocation(location: CLLocation?)
    func failToUpdateLocation(state: GeoLocationState)
    func permissionGiven()
}

public class GeoLocationService: NSObject {

    weak var delegate: GeoLocationServiceDelegate?
    let locationManager: LocationManager
    
    init(locationManager: LocationManager) {
        self.locationManager = locationManager
        super.init()
        locationManager.setDelegate(locationDelegate: self)
    }
    
    deinit {
        self.delegate = nil
    }
    
    func startCheckingLocation(fence: GeoFence, wifiName: String?) {
        if fence.wifiName == wifiName {
            delegate?.locationDetermined(state: .inWifi)
            return
        }

        let circularRegion = CLCircularRegion(center: CLLocationCoordinate2D(latitude: fence.latitude,
                                                                             longitude: fence.longitude),
                                              radius: fence.radius,
                                              identifier: "id")
        
        locationManager.requestState(for: circularRegion)
    }
    
    func requestWhenInUseAuthorization() {
        locationManager.requestWhenInUseAuthorization()
    }
    
    func startGetCurrentLocationInfo() {
        locationManager.requestLocation()
    }
    
    func getAuthorizationStatus() -> CLAuthorizationStatus {
        return locationManager.authorizationStatus()
    }
}

extension GeoLocationService: CLLocationManagerDelegate {
    public func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        switch state {
        case .inside:
            delegate?.locationDetermined(state: .inLocation)
        case .outside:
            delegate?.locationDetermined(state: .outsideLocation)
        default:
            delegate?.locationDetermined(state: .isUnknown)
        }
    }

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            // Location updates are not authorized.
            manager.stopUpdatingLocation()
            delegate?.failToUpdateLocation(state: .noPermission)
            return
        }
        delegate?.failToUpdateLocation(state: .isUnknown)
    }
    
    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        delegate?.didUpdateLocation(location: locations.last)
    }
    
    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            print("Authorized When In Use")
            delegate?.permissionGiven()
        case .authorizedAlways:
            print("Authorized Always")
            delegate?.permissionGiven()
        case .denied, .notDetermined:
            print("Denied")
            delegate?.failToUpdateLocation(state: .noPermission)
        default:
            print("Default Status: \(status)")
        }
    }
}
