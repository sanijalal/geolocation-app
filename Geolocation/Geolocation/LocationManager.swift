//
//  LocationManager.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation
import CoreLocation

protocol LocationManager {
    var delegate: CLLocationManagerDelegate? { get set }
    
    func requestLocation()
    func requestState(for: CLRegion)
    func setDelegate(locationDelegate: CLLocationManagerDelegate)
    func stopUpdatingLocation()
    func requestWhenInUseAuthorization()
    func authorizationStatus() -> CLAuthorizationStatus
}

extension CLLocationManager: LocationManager {
    func setDelegate(locationDelegate: CLLocationManagerDelegate) {
        delegate = locationDelegate
    }
    
    func authorizationStatus() -> CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
}
