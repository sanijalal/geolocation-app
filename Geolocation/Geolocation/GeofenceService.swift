//
//  GeofenceService.swift
//  Geolocation
//
//  Created by Sani on 2/22/20.
//  Copyright © 2020 Sani. All rights reserved.
//

import Foundation

public class GeofenceService {
    let key = "geofence"
    let storage: StorageAble
    
    init(storage: StorageAble) {
        self.storage = storage
    }
    
    func get() -> GeoFence? {
        return storage.retrieve(key: key)
    }
    
    func save(fence: GeoFence) -> Bool {
        return storage.store(item: fence, key: key)
    }
}
